<div align="center">

<img alt="gif-header" src="https://i.pinimg.com/originals/5a/64/63/5a6463e51b284bfcbc34869dc72cf0cd.gif" width="225"/>

<h2 align="center">Flutter Toolbox</h2>

[![Semantic Release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)]()
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)]()

---

<img alt="gif-about" src="https://cdn.dribbble.com/users/1138853/screenshots/4834993/06_08_gif.gif" width="325"/>

<p>A simple docker image that includes usefull tools used in a flutter pipeline</p>

<p>
  <a href="#getting-started">Getting Started</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#versioning">Versioning</a>
</p>

</div>

---

## Table of Contents

<details>
  <ol>
    <li><a href="#tools">Tools</a></li>
    <li><a href="#versioning">Versioning</a></li>
    <li><a href="#troubleshooting">Troubleshooting</a></li>
    <li><a href="#show-your-support">Show your support</a></li>
  </ol>
</details>

## Tools

The following packages are installed:

- Flutter + Dart
- Fastlane

## Versioning

To check the change history, please access the [**CHANGELOG.md**](CHANGELOG.md) file.

## Troubleshooting

If you have any problems, please contact **DevOps Team**.

## Show your support

<div align="center">

Give me a ⭐️ if this project helped you!

<img alt="gif-header" src="https://www.icegif.com/wp-content/uploads/baby-yoda-bye-bye-icegif.gif" width="225"/>

Made with 💜 by **DevOps Team** :wave: inspired on [readme-md-generator](https://github.com/kefranabg/readme-md-generator)

</div>
